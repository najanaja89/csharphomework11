﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Shop;

namespace Homework11
{
    class Program
    {
        static void Main(string[] args)
        {
            Basket cart = new Basket();
            string menu = "";
            while (true)
            {
                Console.WriteLine("Press 1 to add shoes in cart");
                Console.WriteLine("Press 2 to List item from cart");
                Console.WriteLine("Press 3 to delete item from cart");
                Console.WriteLine("Press 4 to buy items from cart");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        Shoes shoes = new Shoes();
                        Console.WriteLine("Enter brand");
                        shoes.Brand = Console.ReadLine();
                        Console.WriteLine("Enter size");
                        shoes.Size = Console.ReadLine();
                        Console.WriteLine("Enter Season");
                        shoes.Season = Console.ReadLine();
                        Console.WriteLine("Enter Color");
                        shoes.Color = Console.ReadLine();
                        cart.AddItem(shoes);
                        //Console.ReadLine();
                        break;

                    case "2":
                        cart.ListCart();
                        Console.ReadLine();
                        break;

                    case "3":
                        cart.RemoveItem();
                        break;

                    case "4":
                        Console.WriteLine($"total sum of order are {cart.Sale()}");
                        Console.ReadLine();
                        break;

                    default: break;
                }
            }
            Console.ReadLine();
        }
    }
}
