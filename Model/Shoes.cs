﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Shoes
    {
        Random random;
        public Guid StoreNumber;
        public string Brand { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string Season { get; set; }
        public double Cost { get; set; }

        public Shoes()
        {
            random = new Random();
            Cost = random.Next(100, 600);
            StoreNumber = Guid.NewGuid();
        }
    }
}
