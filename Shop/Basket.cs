﻿using Model;
using ShopAbstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    public class Basket : ISale
    {
        List<Shoes> basket;
        public Basket()
        {
            basket = new List<Shoes>();
        }

        public void AddItem(Shoes shoes)
        {
            basket.Add(shoes);
        }

        public void RemoveItem()
        {
            string guid = "";
            Console.WriteLine("Enter Guid of shoes");
            guid = Console.ReadLine();
            var removeitem = basket.Single(x => x.StoreNumber.ToString() == guid);
            if (removeitem != null)
            {
                basket.Remove(removeitem);
            }
        }

        public void ListCart()
        {
            foreach (var item in basket)
            {
                Console.WriteLine(item.Brand);
                Console.WriteLine(item.Color);
                Console.WriteLine(item.Season);
                Console.WriteLine(item.Size);
                Console.WriteLine(item.Cost);
                Console.WriteLine(item.StoreNumber);
            }
            Console.WriteLine("------------------------------------");
        }

        public double Sale()
        {
            double cost = 0;
            return cost = basket.Sum(x => x.Cost);
        }
    }
}
